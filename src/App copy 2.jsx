import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import { Icon, Button, IconButton } from "@material-ui/core";

function App() {
  return (
    <div>
      <DeleteIcon color="primary" />
      <Icon>start</Icon>

      <Icon>room</Icon>
      <Button
        variant="contained"
        color="secondary"
        startIcon={<DeleteIcon></DeleteIcon>}
      >
        DELETE WILMER 1
      </Button>

      <Button
        variant="contained"
        color="secondary"
        endIcon={<DeleteIcon></DeleteIcon>}
      >
        DELETE WILMER 2
      </Button>
      <Button>pilas</Button>
      <IconButton aria-label="delete">
        <DeleteIcon></DeleteIcon>
      </IconButton>
    </div>
  );
}

export default App;
