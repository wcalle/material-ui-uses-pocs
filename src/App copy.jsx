import React from "react";
import Button from "@material-ui/core/Button";

function App() {
  return (
    <div className="App">
      <Button variant="contained" color="secondary" size="medium">
        color
      </Button>

      <Button variant="contained" color="primary" fullWidth>
        contained iN THIS TIME THE BUTTON HAS A LONG TEXT
      </Button>
      <p>Parrafo</p>

      <Button
        variant="contained"
        color="secondary"
        href="http://www.google.com"
        size="small"
      >
        GOOGLE.COM CLICK HERE! because it is small button
      </Button>
    </div>
  );
}

export default App;
