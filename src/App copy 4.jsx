import React from "react";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const useStyle = makeStyles({
  buttonPersonalized: {
    background:
      "linear-gradient(90deg, rgba(2,8,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,212,255,1) 100%)",
    border: 0,
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "black",
    height: 90,
    padding: "0 30px",
  },
});

function App() {
  const classes = useStyle();

  return (
    <div>
      <Button className={classes.buttonPersonalized}>
        Personalizando boton: ahora es cuando
      </Button>
    </div>
  );
}

export default App;
