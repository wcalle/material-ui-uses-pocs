import React from "react";
import Typography from "@material-ui/core/Typography";

function App() {
  return (
    <div>
      <Typography variant="h6" color="initial">
        texto en h6
      </Typography>

      <Typography variant="h1" color="primary">
        texto en h1
      </Typography>
      <Typography variant="body1" color="secondary" align="center">
        texto en body1
      </Typography>
    </div>
  );
}

export default App;
