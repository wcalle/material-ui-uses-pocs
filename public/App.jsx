import React from "react";
import Button from "@material-ui/core/Button";
import { ThemeProvider } from "@material-ui/core/styles";
import theme from "./common/themeConfig";

function App() {
  return (
    <div>
      <ThemeProvider theme={theme}>
        <Button variant="contained" color="primary">
          Button from me. Now we are talking.
        </Button>

        <Button variant="contained" color="secondary">
          Button from me 2 2 2 . Now we are talking.
        </Button>
      </ThemeProvider>
    </div>
  );
}

export default App;
